# This code contains major security flaws...

## A brief list of things that should be fixed ASAP

- Pickled objects are passed to and from the browser
  through the flask session store. This is vulnerable to
  code injection.
  - Flask session store should be either:
    - Bypassed entirely, instead opting for a UUID based system where data is stored server side.
    - Verified with a checksum before ever touching main logic.
  - Pickled using YAML, potato potata.


- The REDIS database has no password.
  - It's setup to host only to localhost on the server VM.
  - With default config, this means no backups, no saves.



- The host server itself allows password-based authentication
  - Authentication for users with access to root permissions should only be done via key-based
    authentication.


## Now for the less urgent/could blow up this entire planet stuff.

- The server is still hosted on the Flask development server.
  - This server is not suited for production.
    - No SSL support
    - Cannot serve multiple requests at once
    - Simply slow.
  - The server should be deployed onto a WSGI server like Gunicorn.
    - Site can be found here: https://gunicorn.org/


- There is an issue yet to be troubleshooted, where on first page load, the webAppContainer is completely empty, and a refresh
  is required. After refreshing the container populates. This is likely related to the hacky way the layout is set.
  I have implemented a bandaid fix by using a JS snippet to refresh the page one time on first load.

- The character counter is currently changed by a server-side callback. This is bad for obvious reasons. (ping.)


## Potential quality of life improvements.

```
I mean, to be honest this entire codebase should be nuked and rebuilt if moving into a serious environment,
but we all know it wont be, so I seriously recommend containerizing the entire page,
and rebuilding the layout function.
```

#### Other fixes.

- Switch to a more robust database, e.g. SQL. The choice to use Redis was one of simplicity,
  not intention.

- Redo the viewport stuff. It was hacked together to bandaid some other aspect of the rendering stuff.

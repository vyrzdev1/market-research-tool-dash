import classes.database as database
import classes.post as post
import classes.utils as utils

db = database.DB()
locationData = utils.getLocationDataFromIP('185.69.144.122')
postText = "The founding post... Congrats, you found the 1st ever post on this site!"
postObject = post.Post(postText, locationData)
db.pushPost(postObject, postID="00000001")

#!/usr/bin/env python3
### Dash Imports
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
##################
import classes
import flask
import yaml
##################
############################################## FLASK SERVER ###################################################

flaskapp = flask.Flask(__name__)
geoResponse = dict()


@flaskapp.route("/")
def loadWebApp():
    return flask.redirect("/webapp")

############################################# DASH CODE ######################################################


external_js = [
    "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
]

# App Definitions.
app = dash.Dash(__name__, server=flaskapp, url_base_pathname="/webapp/", suppress_callback_exceptions=True, external_scripts=external_js)
db = classes.database.DB()


# Main Layout Definition, defined as a function to allow for live updates.
def app_layout():
    header = html.H1(className="header", children=None)
    footer = html.P(className="footer", children="Developed by VyrzDev, ben@vyrz.dev")
    finalpage = html.Div(id="page", className="parent", children=[
        header,
        html.Div(id="main2", className="main2", children=[
            html.Div(id="container", className="webAppContainer"),
            html.Button(id="pageSwitch", className="pageChangeButton mdc-button mdc-button--raised", children=[
                html.Span(className="mdc-button__ripple", children=[
                    "Switch Page!"
                ]),
            ]),
        ]),
        footer,
        html.Div(id="placeholder", className="placeholder", n_clicks=0, children=[
            dcc.Interval(
                id='interval-component',
                interval=1*1000,
                n_intervals=0
            )])
        ])
    return finalpage


app.layout = app_layout


# Pushes the previous post.
# Returns the next post as an object.
def nextPost(currentPost):
    db.pushPost(currentPost, postID=currentPost.id)
    newPost = db.getRandomPost()
    return newPost


# Callback for pageSwitch button, if the page has just been loaded, it returns webapppost, else, it returns the next page, flipping between webapppost and webapprespond.
@app.callback(
    [Output("pageSwitch", "children"), Output("container", "children")],
    [Input("pageSwitch", "n_clicks")]
)
def pageSwitch(n_clicks):

    webapppost = html.Div(id="postcontainer", children=[
        html.H1(id="postHeader", className="postHeader", children="Got an opinion? Share it below!"),
        html.Div(id="inputContainer", className="mdc-text-field mdc-text-field--textarea inputContainer", children=[
            dcc.Textarea(id="postInputBox", className="postInput mdc-text-field__input", placeholder="Example: New york style pizza is the best pizza!", autoFocus=True, maxLength=70),
            html.Span(id="charCounter", className="charCounter", children="0/70"),
            html.Span(id="successMessage", className="successMessage", children="")
            ]),
        html.Button(id="postSubmit", className="submitButton", children="Submit."),
    ])
    #####

    def webapprespond(post_id):
        web = html.Div(id="container", children=[
            html.Div(id="textContainer", className="textContainer", children=[
                html.Div(id="postID", className="postID", children=("Post ID: " + post_id)),
                html.Div(id="scoreText", className="scoreText", children="Score:"),
                html.Div(id="postTextContainer", className="postTextContainer", children=[
                    html.Div(id="captionText", className="captionText", children="A user from  ... posted..."),
                    html.Div(id="postText", className="postText"),
                ]),
            ]),
            html.Div(id="buttonContainer", className="thumbButtonContainer", children=[
                html.Button(id="thumbsUp", className="thumbsUpButton", children="Yay"),
                html.Button(id="thumbsDown", className="thumbsDownButton", children="Nay"),
            ]),
        ])
        return web
    #####
    if n_clicks is None:
        container = webapprespond(flask.session["post_id"])
        buttonText = "Share an opinion!"
    else:
        if n_clicks % 2 == 0:
            container = webapprespond(flask.session["post_id"])
            buttonText = "Share an opinion!"
        else:
            container = webapppost
            buttonText = "Vote!"
    return buttonText, container


# Code that runs on page load, sets some starter variables, creates an initial post and saves it to flask.session
@app.callback(
    Output("placeholder", "n_clicks"),
    [Input("interval-component", "n_intervals")]
)
def pageLoad(iter):
    if iter == 0:
        currentPost = db.getRandomPost()
        flask.session["currentPost"] = yaml.dump(currentPost)
        flask.session["post_id"] = currentPost.id
        flask.session["postText"] = currentPost.postText
        if currentPost.locationData.get("region") is None:
            flask.session["location"] = "an Unknown Region"
        else:
            flask.session["location"] = currentPost.locationData.get("region")
        print("LOADED PAGE!!!")
        flask.session["score"] = currentPost.score
        flask.session["up_clicks"] = 0
        flask.session["down_clicks"] = 0
        flask.session["geoResponse"] = yaml.dump(classes.utils.getLocationDataFromIP(flask.request.remote_addr))
        return "Loaded"
    else:
        return None


# This callback is triggered when the postID div changes.
# It simply outputs the post's text and score to the screen.
@app.callback(
    [Output("captionText", "children"), Output("postText", "children"), Output("scoreText", "children")],
    [Input("postID", "children")]
)
def displayPostText(postID):
    postText = flask.session["postText"]
    scoreText = "Score: " + str(flask.session["score"])
    captionText = "A user from " + flask.session["location"] + " said..."
    return captionText, postText, scoreText


# Callback from the thumbsUp and thumbsDown buttons.
# If the button has been clicked, this callback will change the score, log the location data, and send that post back to the database.
# It will then load a new post and modify the postID div, this triggers the displayPostText() callback.
@app.callback(
    Output("postID", "children"),
    [Input("thumbsUp", "n_clicks"), Input("thumbsDown", "n_clicks")]

)
def buttonClick(up_clicks, down_clicks):
    previous_up_clicks = flask.session["up_clicks"]
    previous_down_clicks = flask.session["down_clicks"]
    if up_clicks is None or down_clicks is None:
        if up_clicks is None:
            up_clicks = 0
            previous_up_clicks = 0
        if down_clicks is None:
            down_clicks = 0
            previous_down_clicks = 0
    if up_clicks > previous_up_clicks or down_clicks > previous_down_clicks:
        geoResponse = yaml.load(flask.session["geoResponse"])
        print(geoResponse)
        currentPost = yaml.load(flask.session["currentPost"])
        if up_clicks > previous_up_clicks:
            currentPost.thumbsUp(geoResponse)
        elif down_clicks > previous_down_clicks:
            currentPost.thumbsDown(geoResponse)
        currentPost = nextPost(currentPost)
        flask.session["post_id"] = currentPost.id
        flask.session["postText"] = currentPost.postText
        if currentPost.locationData.get("region") is None:
            flask.session["location"] = "an Unknown Region"
        else:
            flask.session["location"] = currentPost.locationData.get("region")
        flask.session["score"] = currentPost.score
        print(currentPost.log)
        currentPost = yaml.dump(currentPost)
        flask.session["currentPost"] = currentPost
        flask.session["up_clicks"], flask.session["down_clicks"] = up_clicks, down_clicks
        return flask.session["post_id"]
    else:
        return flask.session["post_id"]


# Callback from the post submit button.
@app.callback(
    [Output("successMessage", "children"), Output("charCounter", "children")],
    [Input("postSubmit", "n_clicks"), Input("postInputBox", "value")]
)
def submitPost(n_clicks, postText):
    charCount = str(len(postText))
    if n_clicks is None:
        flask.session["submit_n_clicks"] = 0
        return None, charCount+"/70"
    else:
        previous_n_clicks = flask.session["submit_n_clicks"]
        if n_clicks > previous_n_clicks:
            if len(postText) > 10:
                newPost = classes.post.Post(postText, classes.utils.getLocationDataFromIP(addr=flask.request.remote_addr))
                db.pushPost(newPost)
                flask.session["submit_n_clicks"] = n_clicks
                return "Post Uploaded!", charCount+"/70"
            else:
                flask.session["submit_n_clicks"] = n_clicks
                return "Not long enough, must be at least 10 characters.", charCount+"/70"
        else:
            flask.session["submit_n_clicks"] = n_clicks
            return charCount+"/70"


flaskapp.secret_key = "lUofNfTChDrSDiKbeRwbDsaEJLPKaXNf"
flaskapp.run(host="0.0.0.0", port=80)

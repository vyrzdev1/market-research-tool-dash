document.addEventListener('click', function(e) { if(document.activeElement.toString() == '[object HTMLButtonElement]'){ document.activeElement.blur(); } })
$(document).ready(function () {
  'use strict';

  var orientationChange = function () {
    var $element = $('.selector');
    $element.css('height', '100vh'); // Change this to your own original vh value.
    $element.css('height', $element.height() + 'px');
  };

  var s = screen;
  var o = s.orientation || s.msOrientation || s.mozOrientation;
  o.addEventListener('change', function () {
    setTimeout(function () {
      orientationChange();
    }, 250);
  }, false);
  orientationChange();
});
// Sets viewport meta.
var metaTag=document.createElement('meta');
metaTag.name = "viewport"
metaTag.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0"
document.getElementsByTagName('head')[0].appendChild(metaTag);


// Fixed viewport size changing on keyboard render, this does this by dynamically converting vh and vw sizes to px on page load.
setTimeout(function () {
    var viewheight = $(window).height();
    var viewwidth = $(window).width();
    var viewport = $("meta[name=viewport]");
    viewport.attr("content", "height=" + viewheight + "px, width=" +
    viewwidth + "px, initial-scale=1.0");
}, 300);


// Refrshes the page once automatically on first load. This is a band-aid fix for the empty container bug.
(function()
{
  if( window.localStorage )
  {
    if( !localStorage.getItem('firstLoad') )
    {
      localStorage['firstLoad'] = true;
      window.location.reload();
    }
    else
      localStorage.removeItem('firstLoad');
  }
})();

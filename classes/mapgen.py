import addfips
af = addfips.AddFIPS()


def getCountyCodeFromLocData(locData):
    if locData["country"] == "US":
        locData = af.get_state_fips(state_field="region")
        return locData
    else:
        pass


def generateTuple(yeaornay, locData):
    fipsCode = getCountyCodeFromLocData(locData)
    return yeaornay, fipsCode


def generateDataSet(listOfLocDataAndYeaOrNayTuples):
    dataSet = list()
    for x in listOfLocDataAndYeaOrNayTuples:
        dataSet.append(generateTuple(x))
    return dataSet

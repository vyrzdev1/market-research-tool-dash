import redis
import pickle
import random
postIDKey = "postIDs"  # The key for the hash in the redis table that stores postIDs.


class DB:
    def __init__(self, hostname="127.0.0.1", port=6379, password=None):
        self.database = redis.Redis(host=hostname, port=port, password=password)
        # ^^ Initialises a redis DB

    def listPostIDs(self, postIDKey="postIDs"):
        postIDs = self.database.smembers(postIDKey)  # Review!!
        postIDs = [x.decode("utf-8") for x in postIDs]
        return postIDs
        # Pulls and decodes from bytes to str a list of all IDs stored.

    def getNewPostID(self, postIDKey=None):
        postIDs = self.listPostIDs(postIDKey=postIDKey)
        if postIDs is None:
            newPostID = "00000001"
        else:
            newPostID = str(max([int(x) for x in postIDs])+1).zfill(8)
        return newPostID
        # THIS CODE SHOULD BE REVIEWED!!!!
        # Suitable for a Minimum Viable Product, however, postIDs should always be a randomized hash!
        # Iterating like this could cause an issue!
        # The length of the ID is also hardcoded. This should be changed, or this code will eventually break.

    def pushPost(self, postObject, postID=None):
        if postID is None:
            postID = self.getNewPostID(postIDKey=postIDKey)
            postObject.id = postID
        else:
            pass
        postObjectEncoded = pickle.dumps(postObject)
        self.database.set(postID, postObjectEncoded)
        self.database.sadd(postIDKey, postID)
        # Takes postID, stores an encoded Post object in that IDs key, adds this ID to a set of other IDs to prevent ID conflicts.

    def getPost(self, postID):
        postObjectEncoded = self.database.get(postID)
        postObject = pickle.loads(postObjectEncoded)
        return postObject
        # Fetches the encoded post object, decodes it, and returns it.

    def deletePost(self, postID):
        self.database.delete(postID)
        self.database.srem(postIDKey, postID)
        # Erases both the post data, and its ID reference.

    def getRandomPost(self):
        posts = self.listPostIDs()
        print(posts)
        postID = random.choice(posts)
        post = self.getPost(postID)
        post.id = postID
        return post

import datetime


class Post:
    def __init__(self, postText, locationData):
        self.postText = postText
        self.locationData = locationData
        self.log = list()  # Data in log stored in struct: key: + or -, returns a tuple of str(locationData), str(date)
        self.score = int()
        self.log.append(("created", locationData, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

    def thumbsUp(self, locationData):
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.log.append(("yea", locationData, date))
        self.score = self.score + 1

    def thumbsDown(self, locationData):
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.log.append(("nay", locationData, date))
        self.score = self.score - 1

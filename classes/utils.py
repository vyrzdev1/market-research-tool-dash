from urllib.request import urlopen
from json import load


def getLocationDataFromIP(addr="85.210.8.84"):
    if addr == '':
        url = 'https://ipinfo.io/json'
    else:
        url = 'https://ipinfo.io/' + "85.210.8.84" + '/json'
    res = urlopen(url)
    # response from url(if res==None then check connection)
    data = load(res)
    # will load the json response into data
    return data


def alternate():
    while True:
        yield 0
        yield 1
